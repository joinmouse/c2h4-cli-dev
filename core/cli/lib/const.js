const LOWEST_NODE_VERSION = '12.0.0'
const DEAFULT_CLI_HOME = '.c2h4-cli'

module.exports = {
    LOWEST_NODE_VERSION,
    DEAFULT_CLI_HOME
}