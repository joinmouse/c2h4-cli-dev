'use strict';

module.exports = core;

const { homedir } = require('os')
const fs = require('fs')
const path = require('path')

const semver = require('semver')
const colors = require('colors/safe')

const pkg = require('../package.json')
const log = require('@c2h4-cli-dev/log')
const constant = require('./const')

let args, config, userHome

async function core() {
    try {
        // 检查版本号
        checkPkgVersion()
        // 检查node版本
        checkNodeVersion()
        // 检查root账号
        checkRoot()
        // 检查用户目录
        checkUserHome()
        // 检查入参
        checkInputArgs()
        log.verbose('debug', 'test debug log')
        // 检查环境变量
        checkEnv()
        // 检查版本是否为最新
        await checkGlobalUpdate()
    }catch(e) {
        log.error(e.message)
    }
}

function checkPkgVersion() {
    log.info('cli', pkg.version)
}

function checkNodeVersion() {
    const currentVersion = process.version
    const lowestVersion = constant.LOWEST_NODE_VERSION
    if(!semver.gte(currentVersion, lowestVersion)) {
        throw new Error(colors.red(`c2h4-cli-dev 需要安装 v${lowestVersion} 以上版本的node`))
    }
}

function checkRoot() {
    import('root-check').then(res => {
        res.default()
    })
}

function checkUserHome() {
    userHome = homedir()
    if(!userHome || !fs.existsSync(userHome)) {
        throw new Error(colors.red("当前登录用户主目录不存在"))
    }
}

function checkInputArgs() {
    const minimist = require('minimist')
    args = minimist(process.argv.slice(2))
    checkArgs()
}

function checkArgs() {
    if(args.debug) {
        process.env.LOG_LEVEL = 'verbose'
    }else {
        process.env.LOG_LEVEL = 'info'
    }
    log.level = process.env.LOG_LEVEL
}

function checkEnv() {
    const dotenv = require('dotenv')
    const dotenvPath = path.resolve(userHome, '.env')
    if(fs.existsSync(dotenvPath)) {
        dotenv.config({
            path: dotenvPath
        })
    }
    config = createDefaultConfig()
    log.verbose("环境变量", process.env.CLI_HOME_PATH)
}

function createDefaultConfig() {
    const cliConfig = {
        home: userHome
    }
    if(process.env.CLI_HOME) {
        cliConfig['cliHome'] = path.join(userHome, process.env.CLI_HOME)
    }else {
        cliConfig['cliHome'] = path.join(userHome, constant.DEAFULT_CLI_HOME)
    }
    process.env.CLI_HOME_PATH = cliConfig.cliHome
}

async function checkGlobalUpdate() {
    // 1、获取当前版本号和模块名
    const currentVersion = pkg.version
    const npmName = pkg.name
    // 2、npm info module versions
    const { getNpmSemverVersions } = require('@c2h4-cli-dev/get-npm-info')
    const lastVersion = await getNpmSemverVersions(currentVersion, npmName)
    // 3、更新提示
    if(lastVersion && !semver.gt(lastVersion, currentVersion)) {
        log.warn(colors.yellow(`请手动更新 ${npmName} 当前版本 ${currentVersion}, 最新版本 ${lastVersion}， 
更新命令 npm install -g ${npmName}
        `))
    }
}